package com.formationandroid.demoappkotlin.metier.ws


import com.formationandroid.demoappkotlin.bo.Vehicule
import retrofit2.Call
import retrofit2.http.GET

interface WSInterface
{

    // appel get :
    @GET("api/vehicule/1/")
    fun getVehicule1(): Call<Vehicule>

}