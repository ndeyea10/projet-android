package com.formationandroid.demoappkotlin.activites

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.formationandroid.demoappkotlin.R
import com.squareup.picasso.Picasso

class PicassoActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picasso)

        val imageView: ImageView = findViewById(R.id.imageview)
        Picasso.get()
            .load("http://s519716619.onlinehome.fr/exchange/madrental/get-vehicules.php")
            .fit()
            .centerCrop()
            .into(imageView)
    }

}