package com.formationandroid.demoappkotlin.activites

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.formationandroid.demoappkotlin.R
import com.formationandroid.demoappkotlin.bo.Vehicule
import com.formationandroid.demoappkotlin.metier.ws.ReseauHelper
import com.formationandroid.demoappkotlin.metier.ws.RetrofitSingleton
import com.formationandroid.demoappkotlin.metier.ws.WSInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random

class RetrofitActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retrofit)
    }

    fun clicVehicule(view: View)
    {
        // vérification :
        if (!ReseauHelper.estConnecte(this))
        {
            Toast.makeText(this, "Aucune connexion internet !", Toast.LENGTH_LONG).show()
            return
        }

        // appel :
        val service = RetrofitSingleton.retrofit.create(WSInterface::class.java)
        val call = service.getVehicule1()
        call.enqueue(object : Callback<Vehicule>
        {
            override fun onResponse(call: Call<Vehicule>, response: Response<Vehicule>)
            {
                if (response.isSuccessful)
                {

                    var results: List<Vehicule>? = ArrayList()


                    if (results != null)
                    {
                        val vehicule = results[Random.nextInt(results.size - 1)]
                    }

                }
            }
            override fun onFailure(call: Call<Vehicule>, t: Throwable)
            {
                Log.e("tag", "${t.message}")
            }
        })
    }

}